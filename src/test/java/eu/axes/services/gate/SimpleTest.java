package eu.axes.services.gate;

import gate.util.GateException;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;


/**
 * @author ymombrun
 * @date 2014-11-21
 */
@SuppressWarnings({ "javadoc", "static-method" })
public class SimpleTest {


	@Test
	public void loadingCXFFile() throws Exception {
		Assert.assertNotNull(new XmlBeanFactory(new FileSystemResource("src/main/webapp/WEB-INF/cxf-servlet.xml")).getBeansOfType(GateException.class));
	}

}
