package eu.axes.services.gate;

import gate.AnnotationSet;
import gate.Corpus;
import gate.Document;
import gate.Factory;
import gate.creole.ontology.OConstants.RDF;
import gate.creole.ontology.OConstants.RDFS;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.ow2.weblab.core.extended.comparator.SegmentComparator;
import org.ow2.weblab.core.extended.exception.WebLabCheckedException;
import org.ow2.weblab.core.extended.factory.SegmentFactory;
import org.ow2.weblab.core.extended.factory.WebLabResourceFactory;
import org.ow2.weblab.core.extended.ontologies.WebLabProcessing;
import org.ow2.weblab.core.extended.util.ResourceUtil;
import org.ow2.weblab.core.extended.util.TextUtil;
import org.ow2.weblab.core.helper.PoKHelper;
import org.ow2.weblab.core.helper.impl.JenaPoKHelper;
import org.ow2.weblab.core.model.Annotation;
import org.ow2.weblab.core.model.LinearSegment;
import org.ow2.weblab.core.model.Resource;
import org.ow2.weblab.core.model.Text;
import org.ow2.weblab.service.gate.converter.GateConverter;

/**
 *
 * This class is a custom implementation of GateConverter dedicated to AXES types.
 * It enables the creation of AXES instances of NamedEntities in each Text section of the input WebLab Resource.
 */
public class GateToAxes implements GateConverter {


	/**
	 * The base URI of the created instances instances.
	 */
	public final static String AXES_TYPE_PREFIX = "http://axes-project.eu/v1.0/";


	/**
	 * The URI of the service to be added in each annotation created (or null if nothing shall be added).
	 */
	private final String serviceURI;


	private final Log log;


	/**
	 * Create an instance of converter that will not annotate with producer.
	 */
	public GateToAxes() {
		this(null);
	}


	/**
	 * @param serviceURI
	 *            The uri to be used in produced by statement that describe the created annotations. Might be null, in this case not produced by statement will be added.
	 */
	public GateToAxes(final String serviceURI) {
		this.serviceURI = serviceURI;
		this.log = LogFactory.getLog(this.getClass());
	}


	@Override
	public void convertInformation(final Corpus corpusGate, final Resource resource, final Map<Document, Text> gateDocsAndText) {

		// Creates the annotation that will contains the information extracted from Gate
		final Annotation docAnnot = WebLabResourceFactory.createAndLinkAnnotation(resource);

		/*
		 * Creates the helper that will be used.
		 * Set the autoCommit mode to false to prevent from a lot of useless serializations.
		 * Add some prefixes to have a "beautiful" RDF.
		 */
		final PoKHelper docPoKhelper = new JenaPoKHelper(docAnnot, false);
		docPoKhelper.setNSPrefix("wlp", WebLabProcessing.NAMESPACE);
		docPoKhelper.setNSPrefix("axes", GateToAxes.AXES_TYPE_PREFIX);

		// Add is producedBy statement if needed
		if (this.serviceURI != null) {
			docPoKhelper.createResStat(docAnnot.getUri(), WebLabProcessing.IS_PRODUCED_BY, this.serviceURI);
		}

		final Iterator<Entry<gate.Document, Text>> it = gateDocsAndText.entrySet().iterator();
		int nbAnnots = 0;
		while (it.hasNext()) {
			final Map.Entry<gate.Document, Text> entry = it.next();
			nbAnnots += this.linkGateAnnotsToText(docPoKhelper, resource.getUri(), entry.getValue(), entry.getKey().getAnnotations());

			this.log.debug("Number of segment after GateExtractionComponent: " + entry.getValue().getSegment().size());
			if (this.log.isTraceEnabled()) {
				try {
					this.log.trace(ResourceUtil.saveToXMLString(entry.getValue()));
				} catch (final WebLabCheckedException wlce) {
					this.log.warn("Unable to serialise to XML the resource: '" + entry.getValue().getUri() + "'.", wlce);
				}
			}

			// Empties the memory for each doc
			corpusGate.unloadDocument(entry.getKey());
			Factory.deleteResource(entry.getKey());
			it.remove();
		}

		if (nbAnnots > 0) {
			// Final commit of the extracted RDF. A RuntimeException might occur in case of bad syntax. Catch it to prevent the whole not document to be annotated
			try {
				docPoKhelper.commit();
			} catch (final Exception e) {
				this.log.error("Unable to serialise RDF model for document '" + resource.getUri() + "'.", e);
				resource.getAnnotation().remove(docAnnot);
			}

		} else {
			// Remove useless annotation
			resource.getAnnotation().remove(docAnnot);
		}


		// Clears the map to get memory back
		gateDocsAndText.clear();

		// Empties the memory from the corpus
		Factory.deleteResource(corpusGate);
	}


	/**
	 * Annotate text with each annotation in annotation set.
	 * At the end, sorts the segments list to ease further process.
	 * 
	 * @param docPoKhelper
	 *            The PoKHelper opened on the document.
	 * @param text
	 *            The WebLab Text to be annotated
	 * @param annots
	 *            The Gate annotation set to be used to annotate text
	 * @return The number of entities annotated
	 */
	private int linkGateAnnotsToText(final PoKHelper docPoKhelper, final String docUri, final Text text, final AnnotationSet annots) {
		int nbAnnots = 0;

		// Creates the annotation that will contains the information extracted from Gate
		final Annotation wlAnnot = WebLabResourceFactory.createAndLinkAnnotation(text);

		/*
		 * Creates the helper that will be used.
		 * Set the autoCommit mode to false to prevent from a lot of useless serializations.
		 * Add some prefixes to have a "beautiful" RDF.
		 */
		final PoKHelper pokhe = new JenaPoKHelper(wlAnnot, false);
		pokhe.setNSPrefix("wlp", WebLabProcessing.NAMESPACE);
		pokhe.setNSPrefix("axes", GateToAxes.AXES_TYPE_PREFIX);

		// Add is producedBy statement if needed
		if (this.serviceURI != null) {
			pokhe.createResStat(wlAnnot.getUri(), WebLabProcessing.IS_PRODUCED_BY, this.serviceURI);
		}

		if (this.log.isDebugEnabled()) {
			this.log.debug("Gate Annotation set: " + annots);
		}

		// For each annotation in the annotation Set
		for (final gate.Annotation gateAnnot : annots) {
			if (gateAnnot.getType().equals("PER") || gateAnnot.getType().equals("Person")) {
				this.linkGateAnnotToTextWithDataProperty(text, gateAnnot, pokhe, docPoKhelper, docUri, "Person", RDFS.LABEL);
				nbAnnots++;
			} else if (gateAnnot.getType().equals("ORG") || gateAnnot.getType().equals("Organization") || gateAnnot.getType().equals("Organisation")) {
				this.linkGateAnnotToTextWithDataProperty(text, gateAnnot, pokhe, docPoKhelper, docUri, "Organization", RDFS.LABEL);
				nbAnnots++;
			} else if (gateAnnot.getType().equals("LOC") || gateAnnot.getType().equals("Location")) {
				this.linkGateAnnotToTextWithDataProperty(text, gateAnnot, pokhe, docPoKhelper, docUri, "Location", RDFS.LABEL);
				nbAnnots++;
			}
		}

		if (nbAnnots > 0) {
			// Final commit of the extracted RDF. A RuntimeException might occur in case of bad syntax. Catch it to prevent the whole not document to be annotated
			try {
				pokhe.commit();
			} catch (final Exception e) {
				this.log.error("Unable to serialise RDF model for text '" + text.getUri() + "'.", e);
				this.log.debug(annots);
				text.getAnnotation().remove(wlAnnot);
			}

			// Sort segments in the right order to have a better usability
			Collections.sort(text.getSegment(), new SegmentComparator());
		} else {
			// Remove useless annotation
			text.getAnnotation().remove(wlAnnot);
		}

		return nbAnnots;
	}



	private void linkGateAnnotToTextWithDataProperty(final Text text, final gate.Annotation annotGate, final PoKHelper pokh, PoKHelper docPoKhelper, final String docUri, final String annotAxesType,
			final String dataProperty) {
		// Creates the segment from start and end of the Gate Annotation
		final LinearSegment segment = SegmentFactory.createAndLinkLinearSegment(text, annotGate.getStartNode().getOffset().intValue(), annotGate.getEndNode().getOffset().intValue());

		// The type of the RDF instance
		final String typeURI = GateToAxes.AXES_TYPE_PREFIX + annotAxesType;

		/*
		 * Try to retrieve the label from the text content.
		 * If it's throws an exception, it means that the segment is not properly set and need to be removed.
		 */
		final String instanceURI;
		final String label;
		try {
			label = TextUtil.getSegmentText(text, segment);
			instanceURI = this.getUriFromLabel(GateToAxes.AXES_TYPE_PREFIX + annotAxesType + '#', label);
		} catch (final WebLabCheckedException wlce) {
			this.log.warn("Unable to retrieve text at segment: " + segment.getUri() + " - " + segment.getStart() + " - " + segment.getEnd() + ". Removing it.", wlce);
			text.getSegment().remove(segment);
			return;
		}

		// Add simple statements: type, refersTo and label to both text and document
		if (!label.trim().isEmpty()) {
			pokh.createLitStat(instanceURI, dataProperty, label.trim());
			pokh.createResStat(instanceURI, RDF.TYPE, typeURI);
			pokh.createResStat(segment.getUri(), WebLabProcessing.REFERS_TO, instanceURI);
			docPoKhelper.createLitStat(instanceURI, dataProperty, label.trim());
			docPoKhelper.createResStat(instanceURI, RDF.TYPE, typeURI);
			docPoKhelper.createResStat(docUri, WebLabProcessing.REFERS_TO, instanceURI);
		}



	}


	private String getUriFromLabel(final String baseUri, final String label) {
		final StringBuilder sb = new StringBuilder();
		for (final char c : label.toCharArray()) {
			if (Character.isLetterOrDigit(c)) {
				sb.append(c);
			} else {
				sb.append(' ');
			}
		}
		final String cleanedLabel = sb.toString().replaceAll("\\s+", " ").trim().replace(' ', '_').toLowerCase();
		String uri;
		try {
			uri = new URL(baseUri + cleanedLabel).toURI().toString();
		} catch (final URISyntaxException | MalformedURLException e) {
			this.log.warn("Unable to transform the label '" + label + "' into a uri", e);
			uri = baseUri + System.nanoTime();
		}
		return uri;
	}

}
